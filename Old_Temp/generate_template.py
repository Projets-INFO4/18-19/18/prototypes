# -*- coding: utf-8 -*-
import yaml
import io
import dash
import dash_table as dt
import dash_core_components as dcc
import dash_html_components as html

palette = ('#247ba0','#70c1b3','#b2dbbf','#fbff96','#fbff96')

layout = html.Div(children = [
			  # ENTÊTE
              html.Div(children = [
					html.H6(children='File : '),
					html.H6(children='Regular Expression : ')
					], style={'borderBottom': 'thin lightgrey solid','backgroundColor': palette[3],'padding': '10px 5px'}
			  ),

			  # CHOIX GAUCHE
			  html.Div(children = [
				# Choix de l'abscisse du graphique
	    		html.H6(children='Choose the abscissa to plot the data :'),
	    		dcc.RadioItems(id='choix-abscisse',options=[],value=''),
	   			html.Div(id='output-choix-abscisse'),
	    		# Choix des ordonnees du graphique
			    html.H6(children='Choose the ordinate(s) to plot the data :'),
			    dcc.Checklist(id='choix-ordonnees',values=[])
			  ], style={'width': '49%', 'display': 'inline-block'}),

			  # CHOIX DROIT
			  html.Div([
				html.H6(children='Choose the size of the points :'),
				dcc.Slider(id='choix-taille-points', min=0,max=30,step=0.1,value=10,)
			  ], style={'width': '49%', 'float': 'right', 'display': 'inline-block'}),


			  # DATA 
              html.Div(children = [
              	dcc.Graph(id = 'graphique') 
              ], style = {'float': 'left', 'width': '65%'}),
              html.Div(children = [
              	html.Div(children = 'Gestion de ce que demande l\'utilisateur'),
              ], style = {'float': 'right', 'width': '25%'})
         ])

with io.open('template1.yaml', 'w', encoding='utf8') as outfile:
    yaml.dump(layout, outfile, default_flow_style=False, allow_unicode=True)

print(layout.children[1].children[1].id)