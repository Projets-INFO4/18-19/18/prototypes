# -*- coding: utf-8 -*-
import base64
import datetime

import io
import dash
import dash_core_components as dcc
import dash_html_components as html
import dash_table as dt
from dash.dependencies import Input, Output, State

import pandas as pd
import plotly.graph_objs as go
import regex as re
import sys
import commands
import numpy as np

import yaml


# ********************
# SECURITY VARIABLES
# ********************
isGraph = False
isOptions = False




# ********************
# CSS Style Sheets
# ********************
external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']
app = dash.Dash(__name__, external_stylesheets=external_stylesheets)




# ********************************
# PROCESSING OF PROGRAM ARGUMENTS
# ********************************
if len(sys.argv) != 4 :
	print(" ! It takes 3 arguments:")
	print("\t - the name of the file to process")
	print("\t - the regular expression used to process the file")
	print("\t - the YAML template")
	sys.exit(1)

try:
	fichier = open(sys.argv[1],'r')
except:
	print " ! The data file does not exist."
	sys.exit(1)

try:
  with open(sys.argv[3], 'r') as stream:
	template = yaml.load(stream)
except:
  print " ! The YAML template does not exist."




# *************************************************************************************
# PROCESSING OF FILE DATA (filtering with Regular Expression, storage in a DataFrame)
# *************************************************************************************
line1 = True # True if line 1 processing (column names), False otherwise
line2 = False # True if there is at least 1 line of numeric values
df2_col = []
df2_line = []

for line in fichier:
	res = re.search(sys.argv[2],line)
	if res != None :
	  if line1:
		line1 = False
		for group in res.groups():
		  df2_col.append(group)
	  else:
		current_line = []
		df2_line.append(res.groups())
		line2 = True

if (line2 == False):
	print " ! The processing of the provided data file with the provided regular expression return an empty Data Frame."
	print " ! You may check your data file and/or your regular expression."
	sys.exit(1)

df = pd.DataFrame(data=np.array(df2_line), columns=df2_col)




# ****************************************************
# FUNCTIONS CALL BY THE KEY-WORD OF THE YAML TEMPLATE
# ****************************************************

# Display the name of the data file and the regular expression
def generate_header():
  return  html.Div([
			html.H3(children='File : ' + sys.argv[1]),
			html.H3(children='Regular Expression : ' + sys.argv[2]),
			html.H3(children='YAML Template : ' + sys.argv[3]),
		  ], style={
			'borderBottom': 'thin lightgrey solid',
			'backgroundColor': 'rgb(230, 246, 255)',
			'padding': '10px 5px'
		  })

# Display the options and settings of the graph
# (choice of abscissa and ordinates, linear or logarithmic scales, size of points...)
def generate_options():
  global isOptions
  isOptions = True
  return html.Div([
			html.Div([
				html.H4(children='Choose the abscissa to plot the data :'),
				dcc.RadioItems(
					id='choice-abscissa',
					options=[{'label': df.columns[i], 'value': df.columns[i]} for i in range (0,len(df.columns))],
					value=df.columns[0]
				),
				dcc.RadioItems(
						id='crossfilter-xaxis-type',
						options=[{'label': 'Linear', 'value': 'linear'},
					   {'label': 'Log', 'value': 'log'}
					  ],
						value='linear',
						labelStyle={'display': 'inline-block'}
					),
				html.Div(id='output-choice-abscissa'),

				# Choice of the ordinates of the graph
				html.H4(children='Choose the ordinate(s) to plot the data :'),
				dcc.Checklist(
					id='choice-ordinates',
					values=[]
				),
				dcc.RadioItems(
						id='crossfilter-yaxis-type',
						options=[{'label': 'Linear', 'value': 'linear'},
					   {'label': 'Log', 'value': 'log'}
					  ],
						value='linear',
						labelStyle={'display': 'inline-block'}
					)
			],
				style={'width': '49%', 'display': 'inline-block'}),

			html.Div([ # RIGHT COLUMN
			  html.H4(children='Choose the type of the graph :'),
			  dcc.RadioItems(
				id='choice-mode',
				  options=[
					  {'label': 'Points', 'value': 'markers'},
					  {'label': 'Linked points', 'value': 'marker'},
					  {'label': 'Bar', 'value': 'bar'}
				  ],
				  value='markers'
			  ),

			  html.H4(children='Choose the size of the points :'),
			  dcc.Slider(
				id='choice-taille-points',
				  min=0,
				  max=30,
				  step=0.1,
				  value=10,
			  )
			], style={'width': '49%', 'float': 'right', 'display': 'inline-block'})
		  ])

# Display the data in a table
def generate_table(dataframe, max_rows=len(df2_line)):
	return html.Table(
			  # Header
			  [html.Tr([html.Th(col) for col in dataframe.columns])] +

			  # Body
			  [html.Tr([
				  html.Td(dataframe.iloc[i][col]) for col in dataframe.columns
			  ]) for i in range(min(len(dataframe), max_rows))]
		  )

# Display the data in a graph
def generate_graph():
  global isGraph
  isGraph = True
  return dcc.Graph(id='graph')

def generate_timer():
	return dcc.Interval(
				id='interval-component',
				interval=1*5000,
				n_intervals=0
			)

def generate_div(father,children):
  ind = 0
  for key in children:
	if (key[0:3] == 'div'):
	  (father.children).append(html.Div(children=[]))
	  generate_div(father.children[ind],children.get(key))
	elif (key <= 'graph'):
	  (father.children).append(generate_graph())
	  (father.children).append(generate_timer())
	elif (key <= 'header'):
	  (father.children).append(generate_header())
	elif (key <= 'options'):
	  (father.children).append(generate_options())
	elif (key <= 'table'):
	  (father.children).append(generate_table(df))
	else:
	  print(" ! The keyword " + key + " is not available in the provided YAML template")
	ind += 1




# **********
# MAIN LINES
# Creation of the app.layout
# **********
app.layout = html.Div(children=[])
generate_div(app.layout,template.get("div"))




# **********
# SECURITY
# **********
if (not(isGraph) or not(isOptions)):
  print(" ! ERROR : You must include the keyword 'graph' AND the keyword 'options' in your YAML template")
  sys.exit(1)




# **********
# CALLBACKS
# **********
@app.callback(
	dash.dependencies.Output('output-choice-abscissa', 'children'),
	[dash.dependencies.Input('choice-abscissa', 'value')])
def callback_a(dropdown_value):
	return 'The column {} is selected as the abscissa of the graph.'.format(dropdown_value)

@app.callback(
	dash.dependencies.Output('choice-ordinates', 'options'),
	[dash.dependencies.Input('choice-abscissa', 'value')])
def set_ordinates_options(selected_abscissa):
	y = []
	for i in range (0,len(df.columns)):
		y.append(df.columns[i])
	y.remove(selected_abscissa)
	return [{'label': i, 'value': i} for i in y]

@app.callback(
	dash.dependencies.Output('choice-ordinates', 'values'),
	[dash.dependencies.Input('choice-ordinates', 'options')])
def set_ordinates_value(available_options):
	return available_options[0]

@app.callback(
	dash.dependencies.Output('graph', 'figure'),
	[dash.dependencies.Input('choice-abscissa', 'value'),
	dash.dependencies.Input('choice-ordinates', 'values'),
	dash.dependencies.Input('choice-taille-points', 'value'),
	dash.dependencies.Input('choice-mode', 'value'),
	dash.dependencies.Input('crossfilter-xaxis-type', 'value'),
	dash.dependencies.Input('crossfilter-yaxis-type', 'value'),
	dash.dependencies.Input('interval-component','n_intervals')])
def set_graph(selected_abscissa, selected_ordinates, selected_point_size, selected_mode, selected_xaxis_type, selected_yaxis_type,timer):
	# Processing of the real time data display ------------------------
	try:
		fichier = open(sys.argv[1],'r')
	except:
		print " ! The data file does not exist."
		sys.exit(1)
	line1 = True
	df2_col = []
	df2_line = []
	for line in fichier:
		res = re.search(sys.argv[2],line)
		if res != None :
			if line1:
				line1 = False
				for group in res.groups():
					df2_col.append(group)
			else:
				current_line = []
				df2_line.append(res.groups())
	df = pd.DataFrame(data=np.array(df2_line), columns=df2_col)
	# -----------------------------------------------------------------
	x = []
	for i in range (min(len(df), len(df2_line))):
		x.append(df.iloc[i][selected_abscissa])

	if (selected_mode == 'markers') or (selected_mode == 'marker') :
		selected_type = ''
	elif (selected_mode == 'bar') :
		selected_type = selected_mode
		selected_mode = ''
	else :
		selected_type = ''
		selected_mode = ''

	return {
				'data': [
					{'x': x,
					 'y': df[selected_ordinates[i]],
					 'type': selected_type,
					 'mode': selected_mode,
					 'name': selected_ordinates[i],
					 'opacity': 0.9,
					 'marker': {
						'size': selected_point_size,
						'line': {'width': 0.5, 'color': 'white'}
					 }
					}for i in range (0,len(selected_ordinates))
				],
				'layout': {
					'title': 'Dash Data Visualization',
					'xaxis':{
					  'title': selected_abscissa,
					  'type': selected_xaxis_type
					  },
					  'yaxis':{
						  'type': selected_yaxis_type
					  },
					  'uirevision' : True
				}
			}

if __name__ == '__main__':
	app.run_server(debug=False)