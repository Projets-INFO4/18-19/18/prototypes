import base64
import datetime
import io

import dash
from dash.dependencies import Input, Output, State
import dash_core_components as dcc
import dash_html_components as html
import dash_table

import pandas as pd
import plotly.graph_objs as go

import re
import sys
import commands

import numpy as np

external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

app = dash.Dash(__name__, external_stylesheets=external_stylesheets)

# TRAITEMENT DES ARGUMENTS DU PROGRAMME
if len(sys.argv) != 4 :
    print(" ! Il faut 3 arguments :")
    print("\t - le nom du fichier a traiter ")
    print("\t - l'expression reguliere utilisee pour traiter le fichier")
    print("\t - le separateur")
    sys.exit(1)

try:
    fichier=open(sys.argv[1],'r')
except:
    print " ! Le fichier n'existe pas."
    sys.exit(1)

# TRAITEMENT DES DONNEES DU FICHIER (filtrage avec ER, stockage dans un DF)
line1 = True # True si traitement de la ligne 1 (noms de colonne), False sinon
df2_col = []
df2_line = []

for line in fichier:
    res = re.search(sys.argv[2],line)
    if res != None :
    	if line1:
    		line1 = False
    		for x in res.string.split(sys.argv[3]):
    			df2_col.append(x)
    	else:
    		current_line = []
    		for x in res.string.split(sys.argv[3]):
    			current_line.append(x)
    		df2_line.append(current_line)

#a = commands.getoutput('python PrototypeExpReg.py ' + sys.argv[1] + ' ' + sys.argv[2] + ' > temp.csv')
#csv = 'temp.csv'
#df = pd.read_csv(csv)
df = pd.DataFrame(data=np.array(df2_line), columns=df2_col)

def generate_table(dataframe, max_rows=len(df2_line)):
    return html.Table(
        # Header
        [html.Tr([html.Th(col) for col in dataframe.columns])] +

        # Body
        [html.Tr([
            html.Td(dataframe.iloc[i][col]) for col in dataframe.columns
        ]) for i in range(min(len(dataframe), max_rows))]
    )

def generate_graph(dataframe, max_rows=len(df2_line)):
	x=[]
	for i in range (min(len(dataframe), max_rows)):
		x.append(dataframe.iloc[i][0])

	return dcc.Graph(
	        id='example-graph',
	        figure={
	            'data': [
	                {'x': x,
	                 'y': dataframe[dataframe.columns[i]],
	                 'mode': 'markers',
	                 'name': dataframe.columns[i],
	                 'opacity': 0.9,
                     'marker': {
                        'size': 5,
                        'line': {'width': 0.5, 'color': 'white'}
                     }
                    }for i in range (1,len(dataframe.columns))
	            ],
	            'layout': {
	                'title': 'Dash Data Visualization'
	            }
	        }
	)

app.layout = html.Div([
	# Affichage des donnees sous forme de nuage de points
    generate_graph(df),

    # Affichage des donnees sous forme de tableau
    generate_table(df)
])

if __name__ == '__main__':
    app.run_server(debug=True)