import base64
import datetime
import io

import dash
from dash.dependencies import Input, Output, State
import dash_core_components as dcc
import dash_html_components as html
import dash_table

import pandas as pd
import plotly.graph_objs as go

import re
import sys
import commands

import numpy as np

external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

app = dash.Dash(__name__, external_stylesheets=external_stylesheets)

# TRAITEMENT DES ARGUMENTS DU PROGRAMME
if len(sys.argv) != 3 :
    print(" ! Il faut 2 arguments :")
    print("\t - le nom du fichier a traiter ")
    print("\t - l'expression reguliere utilisee pour traiter le fichier")
    sys.exit(1)

try:
    fichier=open(sys.argv[1],'r')
except:
    print " ! Le fichier n'existe pas."
    sys.exit(1)

# TRAITEMENT DES DONNEES DU FICHIER (filtrage avec ER, stockage dans un DF)
line1 = True # True si traitement de la ligne 1 (noms de colonne), False sinon
df2_col = []
df2_line = []

for line in fichier:
    res = re.search(sys.argv[2],line)
    if res != None :
    	if line1:
    		line1 = False
    		for group in res.groups():
    			df2_col.append(group)
    	else:
    		current_line = []
    		#for x in res.string.split(sys.argv[3]):
    		#	current_line.append(x)
    		df2_line.append(res.groups())

df = pd.DataFrame(data=np.array(df2_line), columns=df2_col)

def generate_table(dataframe, max_rows=len(df2_line)):
    return html.Table(
        # Header
        [html.Tr([html.Th(col) for col in dataframe.columns])] +

        # Body
        [html.Tr([
            html.Td(dataframe.iloc[i][col]) for col in dataframe.columns
        ]) for i in range(min(len(dataframe), max_rows))]
    )

'''
def generate_graph(dataframe, max_rows=len(df2_line)):
	x=[]
	for i in range (min(len(dataframe), max_rows)):
		x.append(dataframe.iloc[i][0])

	return dcc.Graph(
	        id='graphique',
	        figure={
	            'data': [
	                {'x': x,
	                 'y': dataframe[dataframe.columns[i]],
	                 'mode': 'markers',
	                 'name': dataframe.columns[i],
	                 'opacity': 0.9,
                     'marker': {
                        'size': 5,
                        'line': {'width': 0.5, 'color': 'white'}
                     }
                    }for i in range (1,len(dataframe.columns))
	            ],
	            'layout': {
	                'title': 'Dash Data Visualization'
	            }
	        }
	)
'''

app.layout = html.Div([

	# BOX HAUT
	html.Div([

		html.Div([
			html.H6(children='File : ' + sys.argv[1]),
			html.H6(children='Regular Expression : ' + sys.argv[2])
		], style={
        'borderBottom': 'thin lightgrey solid',
        'backgroundColor': 'rgb(225, 225, 225)',
        'padding': '10px 5px'
   		 }),

		# COLONNE GAUCHE
		html.Div([
			# Choix de l'abscisse du graphique
    		html.H6(children='Choose the abscissa to plot the data :'),
    		dcc.RadioItems(
    		    id='choix-abscisse',
    		    options=[{'label': df.columns[i], 'value': df.columns[i]} for i in range (0,len(df.columns))],
    		    value=df.columns[0]
    		),
   			html.Div(id='output-choix-abscisse'),

    		# Choix des ordonnees du graphique
		    html.H6(children='Choose the ordinate(s) to plot the data :'),
		    #dcc.RadioItems(id='choix-ordonnees'),
		    dcc.Checklist(
		        id='choix-ordonnees',
		        values=[]
		    )
		],
        style={'width': '49%', 'display': 'inline-block'}),

		# COLONNE DROITE
		html.Div([
			html.H6(children='Choose the size of the points :'),
			dcc.Slider(
				id='choix-taille-points',
			    min=0,
			    max=30,
			    step=0.1,
			    value=10,
			)
		], style={'width': '49%', 'float': 'right', 'display': 'inline-block'})
	], style={
        'borderBottom': 'thin lightgrey solid',
        'backgroundColor': 'rgb(250, 250, 250)',
        'padding': '10px 5px'
    }),

	# BOX BAS
	html.Div([
		# Affichage des donnees sous forme de nuage de points
	    #generate_graph(df),
	    dcc.Graph(id='graphique'),

	    # Affichage des donnees sous forme de tableau
	    html.H6(children='Here are the data from your file, as this program understood them :'),
	    generate_table(df)
	])
])

@app.callback(
    dash.dependencies.Output('output-choix-abscisse', 'children'),
    [dash.dependencies.Input('choix-abscisse', 'value')])
def callback_a(dropdown_value):
    return 'The column {} is selected as the abscissa of the graph.'.format(dropdown_value)

@app.callback(
    dash.dependencies.Output('choix-ordonnees', 'options'),
    [dash.dependencies.Input('choix-abscisse', 'value')])
def set_ordonnees_options(selected_abscisse):
    y = []
    for i in range (0,len(df.columns)):
        y.append(df.columns[i])
    y.remove(selected_abscisse)
    return [{'label': i, 'value': i} for i in y]

@app.callback(
    dash.dependencies.Output('choix-ordonnees', 'values'),
    [dash.dependencies.Input('choix-ordonnees', 'options')])
def set_ordonnees_value(available_options):
    return available_options[0]

@app.callback(
    dash.dependencies.Output('graphique', 'figure'),
    [dash.dependencies.Input('choix-abscisse', 'value'),
    dash.dependencies.Input('choix-ordonnees', 'values'),
    dash.dependencies.Input('choix-taille-points', 'value')])
def set_graphique(selected_abscisse, selected_ordonnees, selected_point_size):
    x = []
    for i in range (min(len(df), len(df2_line))):
        x.append(df.iloc[i][selected_abscisse])

    return {
                'data': [
                    {'x': x,
                     'y': df[selected_ordonnees[i]],
                     'mode': 'markers',
                     'name': selected_ordonnees[i],
                     'opacity': 0.9,
                     'marker': {
                        'size': selected_point_size,
                        'line': {'width': 0.5, 'color': 'white'}
                     }
                    }for i in range (0,len(selected_ordonnees))
                ],
                'layout': {
                    'title': 'Dash Data Visualization',
                    'xaxis':{
                    	'title': selected_abscisse
                	}
                }
            }

if __name__ == '__main__':
    app.run_server(debug=True)