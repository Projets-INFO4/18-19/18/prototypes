import regex as re
import sys

# doc = https://python.developpez.com/cours/DiveIntoPython/php/frdiveintopython/scripts_and_streams/command_line_arguments.php
# https://www.w3schools.com/python/python_regex.asp#matchobject
# http://apprendre-python.com/page-expressions-regulieres-regular-python
if len(sys.argv) != 3 :
    print(" ! Il faut 2 arguments :")
    print(" - le nom du fichier a traiter ")
    print(" - l'expression reguliere utilisee pour traiter le fichier")
    sys.exit(1)

try:
    fichier=open(sys.argv[1],'r')
except:
    print " ! Le fichier n'existe pas."
    sys.exit(1)

print "La commande : " + sys.argv[2]
tab = []
for line in fichier:
    res = re.search(sys.argv[2],line)
    if res != None:
    	print res
    	print res.groups()
    	tab.append(res)

for group in tab:
	for i in range(1,len(group.groups())+1):
		print group.group(i) 

# expression reguliere pour ne prendre en compte que les deux premieres colonnes:
# "^([a-zA-Z0-9]*)\s([a-zA-Z0-9]*)"

# obtenir les 2emes et troisiemes colonnes:
# "^[a-zA-Z0-9]*\s[a-zA-Z0-9]*\s([a-zA-Z0-9]*)\s([a-zA-Z0-9]*)"
