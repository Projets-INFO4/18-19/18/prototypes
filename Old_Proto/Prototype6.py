import base64
import datetime
import io

import dash
from dash.dependencies import Input, Output, State
import dash_core_components as dcc
import dash_html_components as html
import dash_table

import pandas as pd
import plotly.graph_objs as go

import re
import sys
import commands

import numpy as np

import yaml

external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

app = dash.Dash(__name__, external_stylesheets=external_stylesheets)

# TRAITEMENT DES ARGUMENTS DU PROGRAMME
if len(sys.argv) != 5 :
    print(" ! Il faut 4 arguments :")
    print("\t - le nom du fichier a traiter ")
    print("\t - l'expression reguliere utilisee pour traiter le fichier")
    print("\t - le separateur")
    print("\t - le template YAML a suivre")
    sys.exit(1)

try:
    fichier = open(sys.argv[1],'r')
except:
    print " ! Le fichier n'existe pas."
    sys.exit(1)

# Read YAML file
with open(sys.argv[4], 'r') as stream:
    template = yaml.load(stream)

# TRAITEMENT DES DONNEES DU FICHIER (filtrage avec ER, stockage dans un DF)
line1 = True # True si traitement de la ligne 1 (noms de colonne), False sinon
df2_col = []
df2_line = []

for line in fichier:
    res = re.search(sys.argv[2],line)
    if res != None :
    	if line1:
    		line1 = False
    		for x in res.string.split(sys.argv[3]):
    			df2_col.append(x)
    	else:
    		current_line = []
    		for x in res.string.split(sys.argv[3]):
    			current_line.append(x)
    		df2_line.append(current_line)

df = pd.DataFrame(data=np.array(df2_line), columns=df2_col)

def generate_table(dataframe, max_rows=len(df2_line)):
    return html.Table(
        # Header
        [html.Tr([html.Th(col) for col in dataframe.columns])] +

        # Body
        [html.Tr([
            html.Td(dataframe.iloc[i][col]) for col in dataframe.columns
        ]) for i in range(min(len(dataframe), max_rows))]
    )


# Personalize header
template.children[0].children[0].children = template.children[0].children[0].children + sys.argv[1]
template.children[0].children[1].children = template.children[0].children[1].children + sys.argv[2]

# Modif options
for i in range (0,len(df.columns)) :
	(template.children[1].children[1].options.append({'label': df.columns[i], 'value': df.columns[i]},) )

template.children[1].children[4].values = df.columns[0]

# Table creation from the data
template.children[4].children.append(generate_table(df))



app.layout = html.Div([ template ])

@app.callback(
    dash.dependencies.Output(template.children[1].children[2].id, 'children'),
    [dash.dependencies.Input(template.children[1].children[1].id, 'value')])
def callback_a(dropdown_value):
    return 'The column {} is selected as the abscissa of the graph.'.format(dropdown_value)

@app.callback(
    dash.dependencies.Output(template.children[1].children[4].id, 'options'),
    [dash.dependencies.Input(template.children[1].children[1].id, 'value')])
def set_ordonnees_options(selected_abscisse):
    y = []
    for i in range (0,len(df.columns)):
        y.append(df.columns[i])
    y.remove(selected_abscisse)
    return [{'label': i, 'value': i} for i in y]

@app.callback(
    dash.dependencies.Output(template.children[1].children[4].id, 'values'),
    [dash.dependencies.Input(template.children[1].children[4].id, 'options')])
def set_ordonnees_value(available_options):
    return available_options[0]

@app.callback(
    dash.dependencies.Output(template.children[3].children[0].id, 'figure'),
    [dash.dependencies.Input(template.children[1].children[1].id, 'value'),
    dash.dependencies.Input(template.children[1].children[4].id, 'values'),
    dash.dependencies.Input(template.children[2].children[1].id, 'value')])
def set_graphique(selected_abscisse, selected_ordonnees, selected_point_size):
    x = []
    for i in range (min(len(df), len(df2_line))):
        x.append(df.iloc[i][selected_abscisse])

    return {
                'data': [
                    {'x': x,
                     'y': df[selected_ordonnees[i]],
                     'mode': 'markers',
                     'name': selected_ordonnees[i],
                     'opacity': 0.9,
                     'marker': {
                        'size': selected_point_size,
                        'line': {'width': 0.5, 'color': 'white'}
                     }
                    }for i in range (0,len(selected_ordonnees))
                ],
                'layout': {
                    'title': 'Dash Data Visualization',
                    'xaxis':{
                    	'title': selected_abscisse
                	}
                }
            }

if __name__ == '__main__':
    app.run_server(debug=True)
