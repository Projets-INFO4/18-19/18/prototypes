import base64
import datetime
import io

import dash
from dash.dependencies import Input, Output, State
import dash_core_components as dcc
import dash_html_components as html
import dash_table

import pandas as pd
import plotly.graph_objs as go

external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

app = dash.Dash(__name__, external_stylesheets=external_stylesheets)

def generate_table(dataframe, max_rows=10):
    return html.Table(
        # Header
        [html.Tr([html.Th(col) for col in dataframe.columns])] +

        # Body
        [html.Tr([
            html.Td(dataframe.iloc[i][col]) for col in dataframe.columns
        ]) for i in range(min(len(dataframe), max_rows))]
    )

def generate_graph(dataframe, max_rows=10):
	x=[]
	for i in range (min(len(dataframe), max_rows)):
		x.append(dataframe.iloc[i][0])

	return dcc.Graph(
	        id='example-graph',
	        figure={
	            'data': [
	                {'x': x,
	                 'y': dataframe[dataframe.columns[i]],
	                 'mode': 'markers',
	                 'name': dataframe.columns[i],
	                 'opacity': 0.7,
                     'marker': {
                        'size': 15,
                        'line': {'width': 0.5, 'color': 'white'}
                     }
                    }for i in range (1,len(dataframe.columns))
	            ],
	            'layout': {
	                'title': 'Dash Data Visualization'
	            }
	        }
	)

app.layout = html.Div([
	# Zone de telechargement du fichier
    dcc.Upload(
        id='upload-data',
        children=html.Div([
            'Drag and Drop or ',
            html.A('Select Files')
        ]),
        style={
            'width': '100%',
            'height': '60px',
            'lineHeight': '60px',
            'borderWidth': '1px',
            'borderStyle': 'dashed',
            'borderRadius': '5px',
            
            'textAlign': 'center',
            'margin': '10px'
        },
        # Allow multiple files to be uploaded
        multiple=True
    ),

    # Affichage des donnees
    html.Div(id='output-data-upload'),
])

# ANALYSE DU FICHIER ET AFFICHAGE DES DONNEES
def parse_contents(contents, filename, date):
    content_type, content_string = contents.split(',')

    decoded = base64.b64decode(content_string)
    try:
        if 'csv' in filename:
            # Assume that the user uploaded a CSV file
            df = pd.read_csv(io.StringIO(decoded.decode('utf-8')))
        elif 'xls' in filename:
            # Assume that the user uploaded an excel file
            df = pd.read_excel(io.BytesIO(decoded))

    except Exception as e:
        print(e)
        return html.Div([
            'There was an error processing this file. This program only works with csv or xls files.'
        ])

    return html.Div([
        html.H5(filename),
        html.H6(datetime.datetime.fromtimestamp(date)),

        # Affichage des donnees sous forme de tableau
        generate_table(df),

        # Affichage des donnees sous forme de graphique
        generate_graph(df),
    ])

@app.callback(Output('output-data-upload', 'children'),
              [Input('upload-data', 'contents')],
              [State('upload-data', 'filename'),
               State('upload-data', 'last_modified')])
def update_output(list_of_contents, list_of_names, list_of_dates):
    if list_of_contents is not None:
        children = [
            parse_contents(c, n, d) for c, n, d in
            zip(list_of_contents, list_of_names, list_of_dates)]
        return children

if __name__ == '__main__':
    app.run_server(debug=True)