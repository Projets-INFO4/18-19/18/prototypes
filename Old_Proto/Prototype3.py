import base64
import datetime
import io

import dash
from dash.dependencies import Input, Output, State
import dash_core_components as dcc
import dash_html_components as html
import dash_table

import pandas as pd
import plotly.graph_objs as go

external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

app = dash.Dash(__name__, external_stylesheets=external_stylesheets)

csv = 'Data/data.csv'
df = pd.read_csv(csv)

def generate_table(dataframe, max_rows=10):
    return html.Table(
        # Header
        [html.Tr([html.Th(col) for col in dataframe.columns])] +

        # Body
        [html.Tr([
            html.Td(dataframe.iloc[i][col]) for col in dataframe.columns
        ]) for i in range(min(len(dataframe), max_rows))]
    )


def generate_graph(dataframe, max_rows=10):
	x=[]
	for i in range (min(len(dataframe), max_rows)):
		x.append(dataframe.iloc[i][0])

	return dcc.Graph(
	        id='example-graph',
	        figure={
	            'data': [
	                {'x': x,
	                 'y': dataframe[dataframe.columns[i]],
	                 'mode': 'markers',
	                 'name': dataframe.columns[i],
	                 'opacity': 0.7,
                     'marker': {
                        'size': 15,
                        'line': {'width': 0.5, 'color': 'white'}
                     }
                    }for i in range (1,len(dataframe.columns))
	            ],
	            'layout': {
	                'title': 'Dash Data Visualization'
	            }
	        }
	)

app.layout = html.Div([
    # Affichage des donnees sous forme de tableau
    generate_table(df),

    # Affichage des donnees sous forme de nuage de points
    generate_graph(df)
])

if __name__ == '__main__':
    app.run_server(debug=True)