import base64
import datetime
import io

import dash
from dash.dependencies import Input, Output, State
import dash_core_components as dcc
import dash_html_components as html
import dash_table

import pandas as pd
import plotly.graph_objs as go

import regex as re
import sys
import commands

import numpy as np

external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

app = dash.Dash(__name__, external_stylesheets=external_stylesheets)


# PROCESSING OF PROGRAM ARGUMENTS
if len(sys.argv) != 3 :
    print(" ! It takes 2 arguments:")
    print("\t - the name of the file to process")
    print("\t - the regular expression used to process the file")
    sys.exit(1)

try:
    fichier=open(sys.argv[1],'r')
except:
    print " ! The file does not exist."
    sys.exit(1)

# FILE DATA PROCESSING (filtering with Regular Expression, storage in a DataFrame)
line1 = True # True if line 1 processing (column names), False otherwise
df2_col = []
df2_line = []

for line in fichier:
    res = re.search(sys.argv[2],line)
    if res != None :
    	if line1:
    		line1 = False
    		for group in res.groups():
    			df2_col.append(group)
    	else:
    		current_line = []
    		#for x in res.string.split(sys.argv[3]):
    		#	current_line.append(x)
            #            df2_line.append(current_line)
    		df2_line.append(res.groups())


df = pd.DataFrame(data=np.array(df2_line), columns=df2_col)



def generate_table(dataframe, max_rows=len(df2_line)):
    return html.Table(
        # Header
        [html.Tr([html.Th(col) for col in dataframe.columns])] +

        # Body
        [html.Tr([
            html.Td(dataframe.iloc[i][col]) for col in dataframe.columns
        ]) for i in range(min(len(dataframe), max_rows))]
    )

'''
def generate_graph(dataframe, max_rows=len(df2_line)):
	x=[]
	for i in range (min(len(dataframe), max_rows)):
		x.append(dataframe.iloc[i][0])

	return dcc.Graph(
	        id='graph',
	        figure={
	            'data': [
	                {'x': x,
	                 'y': dataframe[dataframe.columns[i]],
	                 'mode': 'markers',
	                 'name': dataframe.columns[i],
	                 'opacity': 0.9,
                     'marker': {
                        'size': 5,
                        'line': {'width': 0.5, 'color': 'white'}
                     }
                    }for i in range (1,len(dataframe.columns))
	            ],
	            'layout': {
	                'title': 'Dash Data Visualization'
	            }
	        }
	)
'''

app.layout = html.Div([

	# TOP BOX
	html.Div([

		html.Div([
			html.H6(children='File : ' + sys.argv[1]),
			html.H6(children='Regular Expression : ' + sys.argv[2])
		], style={
        'borderBottom': 'thin lightgrey solid',
        'backgroundColor': 'rgb(225, 225, 225)',
        'padding': '10px 5px'
   		 }),

		# LEFT COLUMN
		html.Div([
			# Choice of the abscissa of the graph
    		html.H6(children='Choose the abscissa to plot the data :'),
    		dcc.RadioItems(
    		    id='choice-abscissa',
    		    options=[{'label': df.columns[i], 'value': df.columns[i]} for i in range (0,len(df.columns))],
    		    value=df.columns[0]
    		),
    		dcc.RadioItems(
                id='crossfilter-xaxis-type',
                options=[{'label': 'Linear', 'value': 'linear'},
					     {'label': 'Log', 'value': 'log'}
					    ],
                value='linear',
                labelStyle={'display': 'inline-block'}
            ),
   			html.Div(id='output-choice-abscissa'),

    		# Choice of the ordinates of the graph
		    html.H6(children='Choose the ordinate(s) to plot the data :'),
		    dcc.Checklist(
		        id='choice-ordinates',
		        values=[]
		    ),
		    dcc.RadioItems(
                id='crossfilter-yaxis-type',
                options=[{'label': 'Linear', 'value': 'linear'},
					     {'label': 'Log', 'value': 'log'}
					    ],
                value='linear',
                labelStyle={'display': 'inline-block'}
            )
		],
        style={'width': '49%', 'display': 'inline-block'}),

		# RIGHT COLUMN
		html.Div([
			html.H6(children='Choose the type of the graph :'),
			dcc.RadioItems(
				id='choice-mode',
			    options=[
			        {'label': 'Points', 'value': 'markers'},
			        {'label': 'Linked points', 'value': 'marker'},
			        {'label': 'Bar', 'value': 'bar'}
			    ],
			    value='markers'
			),

			html.H6(children='Choose the size of the points :'),
			dcc.Slider(
				id='choice-taille-points',
			    min=0,
			    max=30,
			    step=0.1,
			    value=10,
			)
		], style={'width': '49%', 'float': 'right', 'display': 'inline-block'})
	], style={
        'borderBottom': 'thin lightgrey solid',
        'backgroundColor': 'rgb(250, 250, 250)',
        'padding': '10px 5px'
    }),

	# LOWER BOX
	html.Div([
		# Displaying data as a graph
	    #generate_graph(df),
	    dcc.Graph(id='graph'),
        dcc.Interval(
            id='interval-component',
            interval=1*1000,
            n_intervals=0
        ),

	    # Display data as a table
	    html.H6(children='Here are the data from your file, as this program understood them :'),
	    generate_table(df)
	])
])

@app.callback(
    dash.dependencies.Output('output-choice-abscissa', 'children'),
    [dash.dependencies.Input('choice-abscissa', 'value')])
def callback_a(dropdown_value):
    return 'The column {} is selected as the abscissa of the graph.'.format(dropdown_value)

@app.callback(
    dash.dependencies.Output('choice-ordinates', 'options'),
    [dash.dependencies.Input('choice-abscissa', 'value')])
def set_ordinates_options(selected_abscissa):
    y = []
    for i in range (0,len(df.columns)):
        y.append(df.columns[i])
    y.remove(selected_abscissa)
    return [{'label': i, 'value': i} for i in y]

@app.callback(
    dash.dependencies.Output('choice-ordinates', 'values'),
    [dash.dependencies.Input('choice-ordinates', 'options')])
def set_ordinates_value(available_options):
    return available_options[0]

@app.callback(
    dash.dependencies.Output('graph', 'figure'),
    [dash.dependencies.Input('choice-abscissa', 'value'),
    dash.dependencies.Input('choice-ordinates', 'values'),
    dash.dependencies.Input('choice-taille-points', 'value'),
    dash.dependencies.Input('choice-mode', 'value'),
    dash.dependencies.Input('crossfilter-xaxis-type', 'value'),
    dash.dependencies.Input('crossfilter-yaxis-type', 'value'),
    dash.dependencies.Input('interval-component','n_intervals')])
def set_graph(selected_abscissa, selected_ordinates, selected_point_size, selected_mode, selected_xaxis_type, selected_yaxis_type, timer):
    try:
        fichier=open(sys.argv[1],'r')
    except:
        print " ! The file does not exist."
        sys.exit(1)

    # FILE DATA PROCESSING (filtering with Regular Expression, storage in a DataFrame)
    line1 = True # True if line 1 processing (column names), False otherwise
    df2_col = []
    df2_line = []

    for line in fichier:
        res = re.search(sys.argv[2],line)
        if res != None :
            if line1:
                line1 = False
                for group in res.groups():
                    df2_col.append(group)
            else:
                current_line = []
                #for x in res.string.split(sys.argv[3]):
                #   current_line.append(x)
                #            df2_line.append(current_line)
                df2_line.append(res.groups())


    df = pd.DataFrame(data=np.array(df2_line), columns=df2_col)
    x = []
    for i in range (min(len(df), len(df2_line))):
        x.append(df.iloc[i][selected_abscissa])

    if (selected_mode == 'markers') or (selected_mode == 'marker') :
    	selected_type = ''
    elif (selected_mode == 'bar') :
    	selected_type = selected_mode
    	selected_mode = ''
    else :
    	selected_type = ''
    	selected_mode = ''

    return {
                'data': [
                    {'x': x,
                     'y': df[selected_ordinates[i]],
                     'type': selected_type,
                     'mode': selected_mode,
                     'name': selected_ordinates[i],
                     'opacity': 0.9,
                     'marker': {
                        'size': selected_point_size,
                        'line': {'width': 0.5, 'color': 'white'}
                     }
                    }for i in range (0,len(selected_ordinates))
                ],
                'layout': {
                    'title': 'Dash Data Visualization',
                    'xaxis':{
                    	'title': selected_abscissa,
                    	'type': selected_xaxis_type
                	},
                	'yaxis':{
                    	'type': selected_yaxis_type
                	}
                }
            }

if __name__ == '__main__':
    app.run_server(debug=True)